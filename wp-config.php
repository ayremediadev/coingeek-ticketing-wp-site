<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */

define( 'DB_NAME', 'cgweek_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'cgweek_db' );

/** MySQL database password */
define( 'DB_PASSWORD', 'cgweek2019' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'WP_HOME', 'https://coingeekweek.com/wp' );
define( 'WP_SITEURL', 'https://coingeekweek.com/wp' );

define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);

// Disabling WordPress Post Revisions
define('AUTOSAVE_INTERVAL', 300); // seconds
define('WP_POST_REVISIONS', false);

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'OX ax3zfSI-Ru{F7U.1W#IFV)LEEF_5,dpJ*dO7?)SzGswh[8=BwnGnq]SLei;fV' );
define( 'SECURE_AUTH_KEY',   ']Oh3)9@lv4tKH<~R(fpm9:rb.^P~,v.Kx|.S:m8DJrlYp64</T$ge=nG5?yo(#Q(' );
define( 'LOGGED_IN_KEY',     'f.hd`=V;rVJOAT_V.02oZN<;/vp3zGH@IY+5M6% ej,(hKpto-9rTc~:U3-n|=s1' );
define( 'NONCE_KEY',         '9|2|;P9R%D88A.@r!}&b:W>&,7,t!{*K2LeUs=iz4cxRe%4%J>XW0Rr%t9C,Dft!' );
define( 'AUTH_SALT',         '+BS!lX:A+X5>96$T4Ud._jSj(-9Ri%nB*wy!Q!I%1~=&v3^s-*e;*(G2u:Daxwod' );
define( 'SECURE_AUTH_SALT',  'WOIvO_Np)zWk15S,zc<*3n/V~,-$9zX74VPb|; @6v8L@6S<DNP}*}06hz16Ngq|' );
define( 'LOGGED_IN_SALT',    'h{z*FYYW~P>@j^v.c@4`+8,z8=udvwQTb_&eu}/1,_+r$GY(}pAx1jb#G7c7WwCe' );
define( 'NONCE_SALT',        'NR4$uVU$-` {r9V4?g=feebS*BOt`Br{MytuXo)$c]?0?,l#,j^OxZL+PvUO$1)m' );
define( 'WP_CACHE_KEY_SALT', 'iUHgz+ct {uaf_2^!Df;a/Ln<NO:W0Ac,bqWoBp4e#2;*-p5N[BMSb/* _5|hT.p' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'WP_MEMORY_LIMIT', '128M' );
define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
