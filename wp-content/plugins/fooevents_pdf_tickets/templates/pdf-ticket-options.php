<div id="fooevents_pdf_ticket_settings" class="panel woocommerce_options_panel">
    
    <div class="options_group">
            <p class="form-field">
                   <label><?php _e('Email text:', 'fooevents-pdf-tickets'); ?></label>
                   <textarea name="FooEventsPDFTicketsEmailText" id="FooEventsPDFTicketsEmailText"><?php echo esc_attr($FooEventsPDFTicketsEmailText); ?></textarea>
            </p>
    </div>
    <div class="options_group">
            <p class="form-field">
                   <label><?php _e('Ticket footer text:', 'fooevents-pdf-tickets'); ?></label>
                   <textarea name="FooEventsTicketFooterText" id="FooEventsTicketFooterText"><?php echo esc_attr($FooEventsTicketFooterText); ?></textarea>
            </p>
    </div>
</div>
