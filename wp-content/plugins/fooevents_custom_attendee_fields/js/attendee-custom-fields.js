(function($) {
    
    var typing_timer;
    var done_typing_interval = 800;
    
    jQuery('#fooevents_custom_attendee_fields_new_field').on('click', function(){

        fooevents_new_attendee_field();
        
        return false;
        
    });
    
    $('#fooevents_custom_attendee_fields_options_table').on('click', '.fooevents_custom_attendee_fields_remove', function(event) {
       
        fooevents_delete_attendee_field($(this));
        
        return false;
        
    });

    $('#fooevents_custom_attendee_fields_options_table').on('keyup', '.fooevents_custom_attendee_fields_label', function(event) {
        
        clearTimeout(typing_timer);

        typing_timer = setTimeout(fooevents_update_attendee_row_ids, done_typing_interval, $(this));

        return false;
        
    });
    
    $('#fooevents_custom_attendee_fields_options_table').on('keyup', '.fooevents_custom_attendee_fields_options', function(event) {
        
        fooevents_serialize_options();

        return false;
        
    });

    
    $('#fooevents_custom_attendee_fields_options_table').on('keydown', '.fooevents_custom_attendee_fields_label', function(event) {
        
        clearTimeout(typing_timer);
        
    });

    $('#fooevents_custom_attendee_fields_options_table').on('change', '.fooevents_custom_attendee_fields_req', function(event) {

        fooevents_serialize_options();
        
    });
    
    $('#fooevents_custom_attendee_fields_options_table').on('change', '.fooevents_custom_attendee_fields_type', function(event) {

        fooevents_serialize_options();
        fooevents_enable_disable_options($(this));
        
    });
    
    fooevents_serialize_options();
    
})(jQuery);

function fooevents_new_attendee_field() {
    
    var opt_num = jQuery('#fooevents_custom_attendee_fields_options_table tr').length;
    
    var label   = '<input type="text" id="'+opt_num+'_label" name="'+opt_num+'_label" class="fooevents_custom_attendee_fields_label" value="Label_'+opt_num+'" autocomplete="off" maxlength="50" />';
    var type    = '<select id="'+opt_num+'_type" name="'+opt_num+'_type" class="fooevents_custom_attendee_fields_type"><option value="text">Text</option><option value="textarea">Textarea</option><option value="select">Select</option></select>';
    var options = '<input id="'+opt_num+'_options" name="'+opt_num+'_options" class="fooevents_custom_attendee_fields_options" type="text" disabled autocomplete="off" />';
    var req     = '<select id="'+opt_num+'_req" name="'+opt_num+'_req" class="fooevents_custom_attendee_fields_req"><option value="true">Yes</option><option value="false">No</option></select>';
    var remove  = '<a href="#" id="'+opt_num+'_remove" name="'+opt_num+'_remove" class="fooevents_custom_attendee_fields_remove" class="fooevents_custom_attendee_fields_remove">[X]</a>';
    
    var new_field = '<tr id="'+opt_num+'_option" class="fooevents_custom_attendee_fields_option"><td>'+label+'</td><td>'+type+'</td><td>'+options+'</td><td>'+req+'</td><td>'+remove+'</td></tr>';
    jQuery('#fooevents_custom_attendee_fields_options_table tbody').append(new_field);
    
    
}

function fooevents_delete_attendee_field(row) {
    
    row.closest('tr').remove();
    fooevents_serialize_options();
    
}

function fooevents_change_attendee_field_type(row) {
    
     row.closest('.fooevents_custom_attendee_fields_options').remove();
    
}

function fooevents_update_attendee_row_ids(row){
    
    console.log(row);
    
    var row_num = row.closest('tr').index()+1;
    var value = fooevents_encode_input(row.val());

    var new_label_id = value+'_label';
    var new_type_id = value+'_type';
    var new_options_id = value+'_options';
    var new_req_id = value+'_req';
    var new_remove_id = value+'_remove';
    var new_option_id = value+'_option';
 
    fooevents_check_if_label_exists(value);

    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_label').attr("id", new_label_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_label').attr("name", new_label_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_type').attr("id", new_type_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_type').attr("name", new_type_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_options').attr("id", new_options_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_options').attr("name", new_options_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_req').attr("id", new_req_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_req').attr("name", new_req_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_remove').attr("id", new_remove_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_remove').attr("name", new_remove_id);
    jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+')').attr("id", new_option_id);
    
    fooevents_serialize_options();
    
}

function fooevents_encode_input(input) {
    
    var output = input.toLowerCase();
    output = output.replace(/ /g,"_");
    
    return output;
    
}

function fooevents_get_row_option_names() {
    
    var IDs = [];
    jQuery("#fooevents_custom_attendee_fields_options_table").find("tr").each(function(){ IDs.push(this.id); });
    
    return IDs;
    
}

/*function fooevents_check_if_option_exists(value){
    
    value = value+'_option';
    
    var IDs = fooevents_get_row_option_names();
    
    console.log(IDs);
    
    if(jQuery.inArray(value, IDs) !== -1) {
    
        alert('Label is already in use');
    
    }

}*/

function fooevents_check_if_label_exists(value) {
    
    var arr = [];
    $(".fooevents_custom_attendee_fields_label").each(function(){
        var value = $(this).val();
        if (arr.indexOf(value) == -1)
            arr.push(value);
        else
            alert('Label is already in use');
    });
    
}

function fooevents_serialize_options() {
    
    var data={};
    var item_num = 0;
    jQuery('#fooevents_custom_attendee_fields_options_table').find('tr').each(function(){
        var id=jQuery(this).attr('id');
        if(id) {
            var row={};
            jQuery(this).find('input,select,textarea').each(function(){
                row[jQuery(this).attr('name')]=jQuery(this).val();
            });
            data[id]=row;
        }
        
        item_num++;
    });
    
    data = JSON.stringify(data);
    
    jQuery('#fooevents_custom_attendee_fields_options_serialized').val(data);
    
    
}

function fooevents_enable_disable_options(row) {
    
    var row_num = row.closest('tr').index()+1;
    var option_type = jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_type').val();
    
    if(option_type == 'select') {
        
        jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_options').prop("disabled", false);
        
    } else {
        
        jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_options').prop("disabled", true);
        jQuery('#fooevents_custom_attendee_fields_options_table tr:eq('+row_num+') .fooevents_custom_attendee_fields_options').val("");
        
    }
    
    fooevents_serialize_options();
    
}