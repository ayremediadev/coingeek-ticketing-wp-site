<?php
/**
 * Plugin Name: FooEvents Custom Attendee Fields
 * Description: Capture custom attendee information on checkout
 * Version: 1.2.10
 * Author: FooEvents
 * Plugin URI: https://www.fooevents.com/
 * Author URI: https://www.fooevents.com/
 * Developer: FooEvents
 * Developer URI: https://www.fooevents.com/
 * Text Domain: fooevents-custom-attendee-fields
 *
 * Copyright: © 2009-2016 FooEvents.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

//include config
require('config.php');

class Fooevents_Custom_Attendee_Fields {
    
    private $Config;
    private $UpdateHelper;
    
    public function __construct() {
        
        add_action( 'admin_notices', array( $this, 'check_fooevents' ) );
        add_action( 'woocommerce_product_write_panel_tabs', array( $this, 'add_product_custom_attendee_field_options_tab' ) );
        add_action( 'woocommerce_product_data_panels', array( $this, 'add_product_custom_attendee_field_options_tab_options' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'register_styles' ) );
        add_action( 'woocommerce_process_product_meta', array( $this, 'process_meta_box' ) );
        add_action( 'plugins_loaded', array( $this, 'load_text_domain' ) );
        
        $this->plugin_init();
        
    }
    
    /**
     * Initializes plugin
     * 
     */
    public function plugin_init() {
        
        //Main config
        $this->Config = new FooEvents_Custom_Attendee_Fields_Config();
        
        //UpdateHelper
        require_once($this->Config->classPath.'updatehelper.php');
        $this->UpdateHelper = new Fooevents_Custom_Attendee_Fields_Update_Helper($this->Config);
        
    }
    
    /**
     * Initializes the WooCommerce meta box
     * 
     */
    public function add_product_custom_attendee_field_options_tab() {

        echo '<li class="custom_tab_custom_attendee_options"><a href="#fooevents_custom_attendee_field_options">'.__( ' Custom Attendee Fields', 'fooevents-custom-attendee-fields' ).'</a></li>';

    }
    
    /**
     * Add custom attendee field tabs 
     * 
     * 
     */
    public function add_product_custom_attendee_field_options_tab_options() {
        
        global $post;
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($post->ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        
        if(empty($fooevents_custom_attendee_fields_options)) {
            
            $fooevents_custom_attendee_fields_options = array();
            
        }
        
        require($this->Config->templatePath.'custom_attendee_fields_options.php');
        
    }
    
    /**
     * Processes the meta box form once the plubish / update button is clicked.
     * 
     * @global object $woocommerce_errors
     * @param int $post_id
     * @param object $post
     */
    public function process_meta_box($post_id) {
        
        global $woocommerce_errors;

        if(isset($_POST['fooevents_custom_attendee_fields_options_serialized'])) {
            
            $fooevents_custom_attendee_fields_options_serialized = $_POST['fooevents_custom_attendee_fields_options_serialized'];
            update_post_meta($post_id, 'fooevents_custom_attendee_fields_options_serialized', $fooevents_custom_attendee_fields_options_serialized);

        }
        
    }
    
    /**
     * Register plugin scripts.
     * 
     */
    public function register_scripts() {
        
        wp_enqueue_script('jquery');
        wp_enqueue_script('fooevents-custom-attendee-fields-script',  $this->Config->scriptsPath . 'attendee-custom-fields.js', array(), '1.0.0', true);

    }
    
    /**
     * Register plugin styles.
     * 
     */
    public function register_styles() {
        
        wp_enqueue_style( 'fooevents-custom-attendee-fields-style',  $this->Config->stylesPath . 'attendee-custom-fields.css', array(), '1.0.0'  );

    }
    
    /**
     * Checks if FooEvents is installed
     * 
     */
    public function check_fooevents() {
        
        $fooevents_path = WP_PLUGIN_DIR . '/fooevents/fooevents.php';

        $plugin_data = get_plugin_data( $fooevents_path );

        if ( !is_plugin_active( 'fooevents/fooevents.php' ) ) {

            $this->output_notices(array(__( 'The FooEvents Express Check-in plugin requires FooEvents for WooCommerce to be installed.', 'fooevents-express-check-in' )));

        } 

        if ( $plugin_data['Version'] < "1.2.14" ) {

            $this->output_notices(array(__( 'The FooEvents Express Check-in plugin requires FooEvents for WooCommerce version 1.2.14 or newer to be installed.', 'fooevents-express-check-in' )));

        }
        
    }
    
    /**
     * Outputs custom attendee fields on the checkout screen
     * 
     * @param int $product_ID
     * @param int $x
     * @param array $ticket
     * @param object $checkout
     */
    public function output_attendee_fields($product_ID, $x, $y, $ticket, $checkout) {
        
        global $woocommerce;
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        
        $attendeeTerm = get_post_meta($ticket['product_id'], 'WooCommerceEventsAttendeeOverride', true);
        
        
        
        if(empty($attendeeTerm)) {

            $attendeeTerm = get_option('globalWooCommerceEventsAttendeeOverride', true);

        }
        
        if(empty($attendeeTerm) || $attendeeTerm == 1) {

            $attendeeTerm = __('Attendee', 'fooevents-custom-attendee-fields');

        }

        if(!empty($fooevents_custom_attendee_fields_options)) {
            
            foreach($fooevents_custom_attendee_fields_options as $option_key => $option) {

                $option_ids = array_keys($option);
                $option_values = array_values($option);

                $required = ($option_values[3] == 'true')? true : false;
                
                $option_label_output = sprintf(__('%s', 'fooevents-custom-attendee-fields'), ucwords($option_values[0]));
                
                $option_values[0] = $this->_encode_custom_field_name($option_values[0]);
                
                $globalWooCommerceUsePlaceHolders = get_option('globalWooCommerceUsePlaceHolders', true);
                
                if($option_values[1] == 'text') {
                    
                    $textLabel = $option_label_output;
                        
                    $textParams = array(
                        'type'          => 'text',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $textLabel,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $textParams['placeholder'] = $textLabel;

                    }
                    
                    woocommerce_form_field("fooevents_custom_".$option_values[0].'_'.$x.'__'.$y, $textParams, $checkout->get_value("fooevents_custom_".$option_values[0].'_'.$x.'__'.$y ));
                    
                }  elseif($option_values[1] == 'textarea') {
                    
                    $textareaLabel = $option_label_output;
                        
                    $textareaParams = array(
                        'type'          => 'textarea',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $textareaLabel,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders == 'yes') {

                        $textareaParams['placeholder'] = $textareaLabel;

                    }
                    
                    woocommerce_form_field("fooevents_custom_".$option_values[0].'_'.$x.'__'.$y, $textareaParams, $checkout->get_value("fooevents_custom_".$option_values[0].'_'.$x.'__'.$y ));
                    
                } elseif($option_values[1] == 'select') {

                    $select_values = array();
                    if(!empty($option_values[2])) {
                        
                        $options = explode('|', $option_values[2]);

                        foreach($options as $option) {
                            
                            $option = trim($option);
                            $select_values[$option] = $option;
                            
                        }
                        
                    }
                    
                    $selectLabel = $option_label_output;
                        
                    $selectParams = array(
                        'type'          => 'select',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $selectLabel,
                        'placeholder'   => '',
                        'options'       => $select_values,
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders == 'yes') {

                        $selectParams['placeholder'] = $selectLabel;

                    }
                    
                    woocommerce_form_field("fooevents_custom_".$option_values[0].'_'.$x.'__'.$y, $selectParams, $checkout->get_value("fooevents_custom_".$option_values[0].'_'.$x.'__'.$y ));
                    
                }

            }
            
        }
        
    }
    
    /**
     * Checks the custom attendee fields on checkout screen.
     * 
     * @param array $ticket
     * @param int $event
     * @param int $x
     */
    public function check_required_fields($ticket, $event, $x, $y) {
        
        global $woocommerce;
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($ticket['product_id'],'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        
        if(!empty($fooevents_custom_attendee_fields_options)) {
            
            foreach($fooevents_custom_attendee_fields_options as $option_key => $option) {
                
                $option_ids = array_keys($option);
                $option_values = array_values($option);

                if($option_values[3] == 'true') {
                    
                    $field_id = "fooevents_custom_".$this->_encode_custom_field_name($option_values[0]).'_'.$x.'__'.$y;
                    
                    if (empty($_POST[$field_id])) {

                        $notice = sprintf(__( ucfirst($option_values[0]).' is required for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y );
                        wc_add_notice( $notice, 'error' );

                    }
                    
                }
                
            }
            
        }

    }
    
    /**
     * Displays custom attendee details on the tickets detail page.
     * 
     * @param int $post
     */
    public function display_tickets_meta_custom_options($post) {
        
        $post_meta = get_post_meta($post->ID); 
        $custom_values = array();
        
        foreach($post_meta as $key => $meta) {
            
           if (strpos($key, 'fooevents_custom_') === 0) {
               
                $custom_values[$key] = esc_attr($meta[0]);
               
           }
            
        }
        
        if(!empty($custom_values)) {

            require($this->Config->templatePath.'custom-attendee-fields-tickets.php');
            
        }

    }
    
    /**
     * Display custom attendee fields on tickets.
     * 
     * @param int $ID
     * @return array
     */
    public function display_tickets_meta_custom_options_output($ID) {
        
        $post_meta = get_post_meta($ID); 
        $output = '';
        
        foreach($post_meta as $key => $meta) {
            
           if (strpos($key, 'fooevents_custom_') === 0) {
               
                $custom_values[$key] = esc_attr($meta[0]);
                $output .= $this->_output_custom_field_name($key).': '.esc_attr($meta[0])."<br>";
                
           }
            
        }
        
        return $output;
        
    }
    
    /**
     * Displays include custom attendee options 
     * 
     * @param object $post
     * @return string
     */
    public function generate_include_custom_attendee_options($post) {
        
        ob_start();
        
        $WooCommerceEventsIncludeCustomAttendeeDetails = get_post_meta($post->ID, 'WooCommerceEventsIncludeCustomAttendeeDetails', true);

        require($this->Config->templatePath.'include-custom-attendee-details-options.php');

        $custom_attendee_options = ob_get_clean();
        
        return $custom_attendee_options;
        
    }
    
    /**
     * Formats custom attendee fields for CSV
     * 
     * @param int $ID
     * @return array
     */
    public function display_tickets_meta_custom_options_array($ID) {
        
        $post_meta = get_post_meta($ID); 
        $output = array();
        
        foreach($post_meta as $key => $meta) {
            
           if (strpos($key, 'fooevents_custom_') === 0) {
               
                $custom_values[$key] = $meta[0];
                $output[$key] = $meta[0];
                
           }
            
        }
        
        return $output;
        
    }
    
    /**
     * Captures custom attendee options on checkout.
     * 
     * @param int $postID
     * @param int $product_id
     * @param int $x
     */
    public function capture_custom_attendee_options($product_id, $x, $y) {

        $custom_values = array();

        foreach($_POST as $key => $value) {

            if (strpos($key, 'fooevents_custom_') === 0) {

                if (strpos($key, $x.'__'.$y)) {

                    $field_name = preg_replace('/'. preg_quote('_'.$x.'__'.$y, '/') . '$/', '', $key);

                    $custom_values[$field_name] = sanitize_text_field($value);
                    
                }
                
            }
            
        }
        
        return $custom_values;
        
    }
    
    public function process_capture_custom_attendee_options($postID, $WooCommerceEventsCustomAttendeeFields) {

        foreach($WooCommerceEventsCustomAttendeeFields as $key => $value) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $key = esc_attr($key);
                $value = esc_attr($value);
                update_post_meta($postID, $key, $value);

            }
            
        }
        
    }
    
    /**
     * Loads text-domain for localization
     * 
     */
    public function load_text_domain() {

        $path = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
        $loaded = load_plugin_textdomain( 'fooevents-custom-attendee-fields', false, $path);
        
    }
    
    /**
     * Formats field name
     * 
     * @param string $field_name
     * @return string
     */
    private function _output_custom_field_name($field_name) {
        
        $field_name = str_replace('fooevents_custom_', "", $field_name);
        $field_name = str_replace('_', " ", $field_name);
        $field_name = ucwords($field_name);
        
        return $field_name;
        
    }
    
    /**
     * Formats field name
     * 
     * @param string $field_name
     * @return string
     */
    private function _encode_custom_field_name($field_name) {
        
        $field_name = str_replace(' ', '_', $field_name);
        $field_name = strtolower($field_name);
        
        return $field_name;
        
    }
    
    /**
     * Outputs notices to screen.
     * 
     * @param array $notices
     */
    private function output_notices($notices) {

        foreach ($notices as $notice) {

                echo "<div class='updated'><p>$notice</p></div>";

        }

    }
    
}

$Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();