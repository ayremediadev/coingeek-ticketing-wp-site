<div id="fooevents_custom_attendee_field_options" class="panel woocommerce_options_panel" style="display: block;">
    <div class="fooevents_custom_attendee_fields_wrap">
        <div class="options_group">
            <table id="fooevents_custom_attendee_fields_options_table" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Label</th>
                        <th>Type</th>
                        <th>Options</th>
                        <th>Required</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($fooevents_custom_attendee_fields_options as $option_key => $option) :?>
                    <?php $option_ids = array_keys($option); ?>
                    <?php $option_values = array_values($option); ?>
                    <?php $x = 0; $num_option_ids = count($option_ids); $num_option_values = count($option_values); ?>
                    <?php if($num_option_ids == $num_option_values): ?>
                    <tr id="<?php echo $option_key; ?>">
                        <td><input type="text" id="<?php echo $option_ids[0]; ?>" name="<?php echo $option_ids[0]; ?>" class="fooevents_custom_attendee_fields_label" value="<?php echo $option_values[0]; ?>" autocomplete="off" maxlength="150"/></td>
                        <td>
                            <select id="<?php echo $option_ids[1]; ?>" name="<?php echo $option_ids[1]; ?>" class="fooevents_custom_attendee_fields_type" autocomplete="off">
                                <option value="text" <?php echo ($option_values[1] == 'text')? 'SELECTED' : ''; ?>>Text</option>
                                <option value="textarea" <?php echo ($option_values[1] == 'textarea')? 'SELECTED' : ''; ?>>Textarea</option>
                                <option value="select" <?php echo ($option_values[1] == 'select')? 'SELECTED' : ''; ?>>Select</option>
                            </select>
                        </td>
                        <td>
                            <input id="<?php echo $option_ids[2]; ?>" name="<?php echo $option_ids[2]; ?>" class="fooevents_custom_attendee_fields_options" type="text" value="<?php echo $option_values[2]; ?>" <?php echo ($option_values[1] == 'select')? '' : 'disabled'; ?> autocomplete="off" />    
                        </td>
                        <td>
                            <select id="<?php echo $option_ids[3]; ?>" name="<?php echo $option_ids[3]; ?>" class="fooevents_custom_attendee_fields_req" autocomplete="off">
                                <option value="true" <?php echo ($option_values[3] == 'true')? 'SELECTED' : ''; ?>>Yes</option>
                                <option value="false" <?php echo ($option_values[3] == 'false')? 'SELECTED' : ''; ?>>No</option>
                            </select>
                        </td>
                        <td><a href="#" id="<?php echo $option_ids[4]; ?>" name="<?php echo $option_ids[4]; ?>" class="fooevents_custom_attendee_fields_remove" class="fooevents_custom_attendee_fields_remove">[X]</a></td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>    
        </div>
    </div>
    <div id="fooevents_custom_attendee_fields_info">
        <p><a href="#" id="fooevents_custom_attendee_fields_new_field" class='button button-primary'>+ New field</a></p>
        <p class="description">When using the 'Select' option, seperate the options using a pipe symbol. Example: Small|Medium|Large.</p>
    </div>
    <input type="hidden" id="fooevents_custom_attendee_fields_options_serialized" name="fooevents_custom_attendee_fields_options_serialized" value="<?php echo $fooevents_custom_attendee_fields_options_serialized; ?>" autocomplete="off" />
</div>