=== WooZap (Woocommerce + Zapier Integration) ===
Contributors: the-ank
Donate link: http://paypal.me/ankurvishwakarma786
Tags: woocommerce zapier integration,woocommerce and zapier,woocommerce order tracking,woocommerce,zapier,woocommerce + zapier. 
Requires at least: 4.4
Requires PHP: 5.2.4
Tested up to: 4.9.8 
Stable tag: 2.0
License: GPLv2 or later

WooZap (woocommerce + zapier integration) allows admin to integrate woocommerce with zapier api and fetch all the orders info in zapier and transfer to thousands of another zaps.

== Description ==

WooZap (woocommerce + zapier integration) allows admin to integrate woocommerce with zapier api and fetch all the orders info in zapier and transfer to thousands of another zaps.

== New Features == 
1. Bug fixing and functionality improvements.
2. Added trigger button with woozap url field to easily configure the path between woocommerce and zapier.

[youtube https://www.youtube.com/watch?v=Cd-8n5uYp-c]

==Donate==
If you would like to support this plugin , kindly <a href="http://paypal.me/ankurvishwakarma786">Donate us here</a> .

== Suggestions ==
Please do not hesitate to let me know your great suggestions to add on this plugin , also about bugs .

== Installation ==

This section describes how to install the plugin and get it working.

Guide -

1. Extract the zip file.
2. Upload `woozap` directory to the `/wp-content/plugins/` directory .
3. Activate the plugin by the 'Plugins' menu from wp-admin area .
4. Find a new option under Woocommerce > Settings > General to put zapier api url provided by zapier. 
5. Please check attached video to see how it works.


== Frequently Asked Questions ==

= What is zapier ? =

Please visit this url  <a href="https://zapier.com"> https://zapier.com </a>

= How to use this plugin with zapier  ? =

Please have a look on attached video and do not hesitate to ask questions .


== Screen Cast ==
1. Woozap installation and working guide.
[youtube https://www.youtube.com/watch?v=Cd-8n5uYp-c]

== Changelog ==

= 1.0 =
Initial Version released 

= 2.0 =
* Bug Fixing & Functionality Improvements 
* Added trigger button with woozap url field to easily configure the path between woocommerce and zapier.
