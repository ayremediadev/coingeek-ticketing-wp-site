jq2 = jQuery.noConflict();
jq2(function( $ ) {
  var woozap_url_field = $('#woozap_zapier_url');
  var woozap_url_field_parent = woozap_url_field.parent();
  if(woozap_url_field.length > 0){
  	woozap_url_field.after('<button style="padding: 0 10px;margin-left: 20px;height: 32px;" id="woozap_trigger_btn" class="button" >Trigger</button>');
  	woozap_url_field_parent.find('span.description').after('<span style="float:left;"><em style="font-size:12px;color:red"><strong>Note : It is required to click on Trigger button once you have entered and saved the api url in field.</strong></em></span>');
  
  //action

  woozap_url_field_parent.on('click','#woozap_trigger_btn',function(event){
  	event.preventDefault();
  	
  	var woozap_url = woozap_url_field.val();
  	if(woozap_url.length <= 0){
  		alert('Please enter a valid url before apply trigger.');
  		return false;
  	}
  	var params ={
  		'action' : 'trigger_woozap_ajax',
  		'api_url' : woozap_url
  	};
  	$.post(woozap_ajax.ajaxurl,params,function(res){
  		if(res == 'success'){
  			alert('Trigged successfully !');
  		}else{
  			alert('Error while execution , please try again !');
  		}
  	});
  });
  }//if exist
});