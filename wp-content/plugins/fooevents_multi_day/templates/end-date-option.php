<div class="options_group" id="WooCommerceEventsEndDateContainer">
    <p class="form-field">
       <label><?php _e('End Date:', 'fooevents-multiday-events'); ?></label>
       <input type="text" id="WooCommerceEventsEndDate" name="WooCommerceEventsEndDate" value="<?php echo esc_attr($WooCommerceEventsEndDate); ?>"/>
       <img class="help_tip" data-tip="<?php _e('The date that the event is scheduled to end. This is used as a label on the frontend of the website. FooEvents Calendar uses this to display an event spanning multiple days.', 'fooevents-multiday-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
    </p>
</div>

