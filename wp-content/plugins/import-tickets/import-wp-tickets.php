<?php

/**** 
Plugin Name: CSV to WP Tickets
Plugin URI: https://github.com/chrisbrosnan
Description: Convert CSV rows to WP WooCommerce Tickets
Author: Chris Brosnan
Author URI: https://github.com/chrisbrosnan
Version: 1.0
Text Domain: import-wp-tickets
***/

function csv_to_wptickets(){
	$tickets = array(); 
	$args = array(
		'post_type'      => 'event_magic_tickets', //write slug of post type
		'posts_per_page' => -1,
	);
	$query_tickets = new WP_Query( $args );
	if($query_tickets->have_posts()): 
		while($query_tickets->have_posts()) : $query_tickets->the_post(); 
			$i = get_the_ID(); 
			$t = get_the_title(); 
			array_push($tickets, $t);
		endwhile;
	endif;
	wp_reset_query();
    $csv_path = plugin_dir_path( __FILE__ ) . 'Ticket-Export-2019-May-29-1059.csv';
    $file = fopen($csv_path, "r"); 
	while (($line = fgetcsv($file)) !== FALSE) {
		$pars = array(
			'post_title' => $line[1], 
			'post_content' => $line[2], 
			'post_status' => 'publish', 
			'post_author' => $line[33], 
			'post_type' => 'event_magic_tickets', 
		);
		$refn = str_replace('#', '', $line[1]);
		$i = wp_insert_post($pars);
		update_post_meta($i, 'WooCommerceEventsTicketID', $refn);
		update_post_meta($i, 'WooCommerceEventsTicketHash', $line[8]);
		update_post_meta($i, 'WooCommerceEventsProductID', $line[9]);
		update_post_meta($i, 'WooCommerceEventsOrderID', $line[10]);
		update_post_meta($i, 'WooCommerceEventsStatus', $line[12]);
		update_post_meta($i, 'WooCommerceEventsCustomerID', $line[13]);
		update_post_meta($i, 'WooCommerceEventsAttendeeName', $line[14]);
		update_post_meta($i, 'WooCommerceEventsAttendeeLastName', $line[15]);
		update_post_meta($i, 'WooCommerceEventsAttendeeEmail', $line[16]);
		update_post_meta($i, 'WooCommerceEventsVariations', $line[20]);
		update_post_meta($i, 'WooCommerceEventsPurchaserFirstName', $line[22]);
		update_post_meta($i, 'WooCommerceEventsPurchaserLastName', $line[23]);
		update_post_meta($i, 'WooCommerceEventsPurchaserEmail', $line[24]);
		update_post_meta($i, 'WooCommerceEventsPrice', $line[25]);
		update_post_meta($i, 'WooCommerceEventsPriceSymbol', $line[26]);
		update_post_meta($i, 'fooevents_custom_mobile_phone', $line[27]);
		update_post_meta($i, 'WooCommerceEventsProductName', $line[28]);
	}
	fclose($file);
}
register_activation_hook( __FILE__, 'csv_to_wptickets' );

function matchtickets(){
	$c = 0;
	$t = 0;
	$tickets = array(); 
	$args = array(
		'post_type'      => 'event_magic_tickets', //write slug of post type
		'posts_per_page' => -1, 
	);
	$query_tickets = new WP_Query( $args );
	if($query_tickets->have_posts()): 
		while($query_tickets->have_posts()) : $query_tickets->the_post(); 
			$i = get_the_ID(); 
			$t = get_the_title(); 
			array_push($tickets, $t);
		endwhile;
	endif;
	wp_reset_query();
    $csv_path = plugin_dir_path( __FILE__ ) . 'Ticket-Export-2019-May-28-1448.csv';
    $file = fopen($csv_path, "r"); 
	while (($line = fgetcsv($file)) !== FALSE) {
		if(in_array($line[1], $tickets)){
			$c += 1;
			echo 'Matched: ' . $c . '<br/>';
		}
		$t += 1;
	}
	echo 'Total: ' . $t . '<br/>';
	fclose($file);
}
add_shortcode('ticketsmatch', 'matchtickets');